import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit
import requests
import timeit

from flask import Blueprint, render_template, jsonify


def datahub_timeit():
    return requests.head('https://datahub.io')

def datahub_metrics():
    u'''A simple view function'''
    response_time = timeit.timeit(datahub_timeit, number=1)
    return jsonify({
      'ResponseTime': str(response_time * 1000) + ' milliseconds',
      'Website': 'https://datahub.io'
    })

class Onboarding_AbhishekPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IBlueprint)

    # IConfigurer

    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'onboarding_abhishek')

    # IBlueprint

    def get_blueprint(self):
        u'''Return a Flask Blueprint object to be registered by the app.'''
        # Create Blueprint for plugin
        blueprint = Blueprint(self.name, self.__module__)
        blueprint.template_folder = u'templates'
        # Add plugin url rules to Blueprint object
        blueprint.add_url_rule('/onboarding_abhishek', '/onboarding_abhishek', datahub_metrics)
        return blueprint


